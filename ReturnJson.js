var http = require('http');

var app = http.createServer(function(req,res){
    res.setHeader('Content-Type', 'application/json');

    if(req.url == '/'){
      res.write(JSON.stringify({ page: 'Inicial' }));
    }
    else if (req.url == '/friends') {
      res.write(JSON.stringify({ page: 'friends' }));
    }
    else {
      res.write(JSON.stringify({ page: 'Not Found' }));
    }

    res.end();

});
app.listen(3000);

console.log('created Server...');
